package br.edu.up.googleplanilha;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  private void processJson(JSONObject object) {

    // {"v":1.0,"f":"1"},
    // {"v":"Alan"},
    // {"v":"25.253.394-X"},
    // {"v":"213.909.848-00"},
    // {"v":"Solteiro"},
    // {"v":"M"},
    // {"v":"Date(1988,8,6)","f":"06/09/88"},
    // {"v":"São José do Rio Preto"}

    try {

      JSONArray rows = object.getJSONArray("entry");

      for (int r = 0; r < rows.length(); ++r) {

        JSONObject row = rows.getJSONObject(r);
        //JSONArray columns = row.getJSONArray("title");
        JSONObject content = row.getJSONObject("content");
        //Log.d("Google", content);
        String[] dados = content.getString("$t").split(",");

        String id = dados[0].split(":")[1];
        String nome = dados[1].split(":")[1];
        String rg = dados[2].split(":")[1];
        String cpf = dados[3].split(":")[1];
        String estado = dados[4].split(":")[1];
        String sexo = dados[5].split(":")[1];
        String data = dados[6].split(":")[1];
        String local = dados[7].split(":")[1];

        Log.d("Google", "ID: " + id);
        Log.d("Google", "Nome: " + nome);
        Log.d("Google", "RG: " + rg);
        Log.d("Google", "CPF: " + cpf);
        Log.d("Google", "Estado civil: " + estado);
        Log.d("Google", "Sexo: " + sexo);
        Log.d("Google", "Data de nascimento: " + data);
        Log.d("Google", "Local de nascimento: " + local);
      }

//      JSONArray rows = object.getJSONArray("rows");
//
//      for (int r = 0; r < rows.length(); ++r) {
//        JSONObject row = rows.getJSONObject(r);
//        JSONArray columns = row.getJSONArray("c");
//
//        String id = columns.getJSONObject(0).getString("v");
//        String nome = columns.getJSONObject(1).getString("v");
//        String rg = columns.getJSONObject(2).getString("v");
//        String cpf = columns.getJSONObject(3).getString("v");
//        String estado = columns.getJSONObject(4).getString("v");
//        String sexo = columns.getJSONObject(5).getString("v");
//        String data = columns.getJSONObject(6).getString("v");
//        String local = columns.getJSONObject(7).getString("v");
//
//        Log.d("Google", "ID: " + id);
//        Log.d("Google", "Nome: " + nome);
//        Log.d("Google", "RG: " + rg);
//        Log.d("Google", "CPF: " + cpf);
//        Log.d("Google", "Estado civil: " + estado);
//        Log.d("Google", "Sexo: " + sexo);
//        Log.d("Google", "Data de nascimento: " + data);
//        Log.d("Google", "Local de nascimento: " + local);
//      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void onClickGetJSON(View v) {

    new DownloadWebpageTask(new AsyncResult() {
      @Override
      public void onResult(JSONObject object) {
        processJson(object);
      }
    }).execute("https://spreadsheets.google.com/feeds/list/1dicOExIiAS6rnHNgraB2LKt2PaDPGkD2dIw9ehBjbiY/od6/public/basic?alt=json");
    //.execute("https://spreadsheets.google.com/tq?key=1dicOExIiAS6rnHNgraB2LKt2PaDPGkD2dIw9ehBjbiY");
  }

}
